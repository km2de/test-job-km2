# Test Web Project - KM2
---
Test Web Project for Front End Developer position - KM2 Company.

## Table of Contents
---
- [Test Web Project - KM2](#test-web-project---km2)
  - [Table of Contents](#table-of-contents)
  - [About the Project](#about-the-project)
  - [Directory Structure](#directory-structure)
  - [Getting Started](#getting-started)
  - [Built With](#built-with)
    - [Dependencies](#dependencies)
      - [How install Dependencies](#how-install-dependencies)
  - [Authors](#authors)
  - [License](#license)


## About the Project
---
Tasks:
- the layout should be converted to HTML/CSS/Javascript
- the green and blue area should each fill the whole screen area
- the menu on the right should be a static one and show the area, where the user has scrolled to
- for the three teasers on the bottom you can put any link target you like
- create a responsive behavior as you find it most reasonable 
- create a hover effect for the thumbnails
- on click on the thumbnail the overlay should be opened
- please use only vanilla javascript - no library like jQuery, if possible
- the page should be as small as possible to get loaded quickly

## Directory Structure
---
```
┌── build
│   └── .gitkeep
├── src
│   ├── assets
│   │   ├── css
│   │   │   ├── fonts
│   │   │   │   ├── OpenSans-Regular
│   │   │   │   ├── PassagewayBd
│   │   │   │   └── PassagewayLight
│   │   │   └── shame.css
│   │   ├── img
│   │   │   ├── beach.jpg
│   │   │   ├── cat.jpg
│   │   │   ├── cyprus.jpg
│   │   │   ├── sheep.jpg
│   │   │   └── close.svg
│   │   └── js
│   │       ├── vendor
│   │       │   ├── modernizr-3.6.0.min
│   │       │   └── plugins.js
│   │       └── main.js
│   ├── files
│   │   ├── .htaccess
│   │   ├── favicon.ico
│   │   ├── humans.txt
│   │   ├── icon.png
│   │   ├── manifest.json
│   │   ├── robots.txt
│   │   ├── sitemap.xml
│   │   └── smooth.js
│   ├── sass
│   │   ├── _basics
│   │   │   ├── _body-element.sass
│   │   │   ├── _example-buttons.sass
│   │   │   ├── _links.sass
│   │   │   ├── _selection-colors.sass
│   │   │   └── _typography.sass
│   │   ├── _modules
│   │   │   ├── _example-flex-video.sass
│   │   │   ├── _example-fork-tag.sass
│   │   │   └── _example-module-name-lockup.sass
│   │   ├── _pages
│   │   │   ├── _page-home.sass
│   │   │   └── _page-top-experiences.sass
│   │   ├── _tools
│   │   │   ├── _fonts.scss
│   │   │   ├── _mixins.scss
│   │   │   ├── _normalize.scss
│   │   │   └── _vars.sass
│   │   └── main.sass
│   └── views
│       ├── .gitkeep
│       ├── 404.html
│       ├── top-experiences.html
│       └── index.html
├── .gitignore
├── .gitkeep
├── gulpfile.js
├── LICENSE
├── package.json
├── package-lock.json
└── README.md
```

## Getting Started
---
These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

- If needed, [install](http://blog.nodeknockout.com/post/65463770933/how-to-install-node-js-and-npm) `node` and `npm` (Node Package Manager).
- If needed, install `gulp` with `npm install gulp -g`.
- Clone this repo with `git clone https://github.com/minamarkham/sassy-starter` or download the zip.
- In terminal, `cd` to the folder containing your project. Alternatively, you can type `cd ` and drag the location of the folder into your terminal and hit enter (on Macs).
- In terminal, type `npm install`. If (and _only_ if) `npm install` isn't working, try `sudo npm install`. This should install all [dependencies](#dependencies).
- In terminal, enter `gulp`.
- Your browser should open at `http://localhost:3000`. You can access this same page on any device on the same wifi network and they'll see whats on your screen. It'll even sync scrolls and clicks!
- Edit your code inside of the `src` folder.
- Your complied and minified css, html, and javascript files will be created and updated in `dist/`. Never edit files within the `dist/` folder, as it gets deleted frequently.
- To start Gulp, type into console `gulp tasks`
- Keep `gulp` running while you're making changes. When you want to stop the gulp task, hit `ctrl + C`.

	
## Built With
---
- [html5 boilerplate](https://html5boilerplate.com/)
- [Vanilla JS](http://vanilla-js.com/)
- [Modernizr](https://modernizr.com/)
- [Normalize](https://necolas.github.io/normalize.css/)
- [Gulp](https://gulpjs.com/)
- [SASS](https://sass-lang.com/)

### Dependencies
Dependencies that need to be installed for building/using your project
```
`
gulp del
https://www.npmjs.com/package/del

gulp-newer
https://www.npmjs.com/package/gulp-newer

gulp-imagemin
https://www.npmjs.com/package/gulp-imagemin

gulp-sass
https://www.npmjs.com/package/gulp-sass

gulp-uglify
https://www.npmjs.com/package/gulp-uglify

https://www.npmjs.com/package/gulp-concat
gulp-concat

gulp-connect
https://www.npmjs.com/package/gulp-connect

live reload
https://www.npmjs.com/package/gulp-livereload

gulp-autoprefixer

Minify css https://www.npmjs.com/package/gulp-clean-css

gulp-uncss
https://www.npmjs.com/package/gulp-uncss

```
#### How install Dependencies
Instructions for installing the dependencies

`
npm install gulp-newer gulp-imagemin gulp-sass del gulp-uglify gulp-concat gulp-connect gulp-livereload gulp-autoprefixer gulp-clean-css gulp-uncss --save-dev
`

## Authors
---
* **Stanislav Vranic** - *Developing* - [@crowscript](http://crowscript.com)

## License
---
This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details


**[Back to top](#table-of-contents)**
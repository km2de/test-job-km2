// Gulpfile.js configuration

const	gulp = require('gulp'),

// plugins
			del = require('del'),
			newer = require('gulp-newer'),
			imagemin = require('gulp-imagemin'),
			sass = require('gulp-sass'),
			uglify = require('gulp-uglify'),
			concat = require('gulp-concat'),
			prefix = require('gulp-autoprefixer'),
			uncss = require('gulp-uncss'),
			cleanCSS = require('gulp-clean-css'),
			pug = require('gulp-pug'),
			connect = require('gulp-connect'),
			livereload = require('gulp-livereload');

// folders
const	folder = {
				src: 'src/',
				build: 'build/'
			};

// variables
const	files = [
				folder.src + '/files/.*',
				folder.src + '/files/**.*'
			],
			outBuild = folder.build,
			ignore = '!' + outBuild + '.gitkeep',
			prefixerOptions = {
				browsers: ['last 2 versions']
			};

// TASKS

// Clean the build folder
	gulp.task('del', function () {
	  del([
			outBuild + '/*', ignore
	  ], {dot: true});
	});
	
// moving files to build folder
	gulp.task('files', function () {
		return gulp.src(files)
		.pipe(newer(outBuild))
		.pipe(gulp.dest(outBuild))
		.pipe(livereload());
	});
    
// copy all static HTML to build folder
   gulp.task('html', function() {
       return gulp.src(folder.src + 'views/html/*.html')
       .pipe(newer(outBuild))
       .pipe(gulp.dest(outBuild))
				.pipe(livereload());
   });

// set PUG template engine to HTML
		// gulp.task('pug', function buildHTML() {
		// 	return gulp.src(folder.src + 'views/pug/*.pug')
		// 	.pipe(pug({
		// 		// Your options in here.
		// 	}))
		// 	.pipe(gulp.dest(outBuild))
		// 	.pipe(livereload());
		// });

// copy all static CSS to build folder
    gulp.task('css', function() {
        var out = outBuild + 'assets/css/';
        return gulp.src(folder.src + 'assets/css/**/*')
        .pipe(newer(out))
        .pipe(gulp.dest(out))
				.pipe(livereload());
    });

// copy all static JS to build folder
//    gulp.task('js', function() {
//        var out = outBuild + 'assets/js/';
//        return gulp.src(folder.src + 'assets/js/**/*')
//        .pipe(newer(out))
//        .pipe(gulp.dest(out));
//    });

// image processing optimize
    gulp.task('images', function() {
        var out = outBuild + 'assets/img/';
        return gulp.src(folder.src + 'assets/img/**/*')
        .pipe(newer(out))
        .pipe(imagemin())
        .pipe(gulp.dest(out))
				.pipe(livereload());
    });

// sass to build/assetes/css
    gulp.task('sass', function () {
    return gulp.src([folder.src + '/sass/*', '!src/sass/_*/'])
        .pipe(sass().on('error', sass.logError))
				.pipe(prefix(prefixerOptions))
				.pipe(uncss({
								html: [outBuild +'/**/*.html']
						}))
				.pipe(cleanCSS())
        .pipe(gulp.dest(outBuild + '/assets/css'))
				.pipe(livereload());
    });

// Concat JS
	gulp.task('scripts', function() {
		var out = outBuild + 'assets/js/';
		
		return gulp.src([''+ folder.src + 'assets/js/vendor/*.js', ''+ folder.src + 'assets/js/*.js'])
			.pipe(concat('all.js'))
			.pipe(uglify())
			.pipe(gulp.dest(out))
			.pipe(livereload());
	});

// Set live server http://localhost:8001/
    gulp.task('localhost', function() {
        connect.server({
            root: outBuild,
            port: 8001,
            livereload: true
        });
    });

// Live reload server - Watching changes
    gulp.task('watch', function () {
			livereload.listen();
//        gulp.watch([folder.src + 'views/html/*.html'], ['html']);
				gulp.watch([folder.src + 'views/html/*.html'], ['html']);
        gulp.watch([folder.src + 'sass/**/*'], ['sass']);
        gulp.watch([folder.src + 'assets/css/**/*'], ['css']);
        gulp.watch([folder.src + 'assets/js/**/*'], ['js']);
        gulp.watch([folder.src + 'assets/img/**/*'], ['images']);
    });

// Run clear/del task
  gulp.task('clear', ['del']);
  

// Run all Tasks
  gulp.task('tasks', ['html', 'css', 'files', 'images', 'sass', 'scripts', 'localhost', 'watch' ]);
